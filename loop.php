<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>    
        <h1><?php the_title(); ?></h1>
        <?php the_content(); ?>



        the_permalink();

        the_excerpt();

        the_post_thumbnail();

        the_time();

    <?php endwhile; ?>
<?php endif; ?>